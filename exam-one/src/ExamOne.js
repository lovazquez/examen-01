import { html, css, LitElement } from 'lit-element';

export class ExamOne extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--exam-one-text-color);
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number }, 
      label: {type: Number}, 
      counters: {type: Array}
    };
  }

  constructor() {
    super();
    this.title = 'Counter App';
    this.counter = 1;
    this.label = 'El contador vale:'+this.counter;
    this.counters= [this.counter];

  }

  updated(value){
    console.log(value);
  }

  __increment() {
    this.counter += 1;
    this.label = this.label.slice(0,17)+this.counter;
    this.counters.push(this.counter);
  }

  render() {
    return html`
      <h2>${this.title} Nr. ${this.counter}!</h2>
      <h3>${this.label}</h3>
      <button @click=${this.__increment}>increment</button>

      ${this.counters.map(count=>html`<h4></h4> ${count}` )}
    `;
  }
}
